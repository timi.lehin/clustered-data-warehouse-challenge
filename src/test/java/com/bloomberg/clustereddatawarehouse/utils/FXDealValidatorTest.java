package com.bloomberg.clustereddatawarehouse.utils;

import com.bloomberg.clustereddatawarehouse.dtos.FxDealRequestDto;
import com.bloomberg.clustereddatawarehouse.exceptions.InvalidDealRequestException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class FXDealValidatorTest {

    @InjectMocks
    private FxDealValidator fxDealValidator;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testValidateFXDeal_ValidDeal() {
        FxDealRequestDto validDeal = createValidDeal();
        assertThatCode(() -> fxDealValidator.validateFXDeal(validDeal))
                .doesNotThrowAnyException();
    }

    @Test
    void testValidateFXDeal_NullDeal() {
        assertThatThrownBy(() -> fxDealValidator.validateFXDeal(null))
                .hasMessage("FX Deal request should not be null")
                .isInstanceOf(InvalidDealRequestException.class);
    }

    @Test
    void testValidateUniqueId_NullUniqueId() {
        FxDealRequestDto deal = createValidDeal();
        deal.setUniqueId(null);
        assertThatThrownBy(() -> fxDealValidator.validateFXDeal(deal))
                .hasMessage("FX Deal unique ID cannot be null or empty")
                .isInstanceOf(InvalidDealRequestException.class);
    }

    @Test
    void testValidateUniqueId_EmptyUniqueId() {
        FxDealRequestDto deal = createValidDeal();
        deal.setUniqueId("");
        assertThatThrownBy(() -> fxDealValidator.validateFXDeal(deal))
                .hasMessage("FX Deal unique ID cannot be null or empty")
                .isInstanceOf(InvalidDealRequestException.class);
    }

    @Test
    void testValidateCurrency_InvalidCurrency() {
        FxDealRequestDto deal = createValidDeal();
        deal.setFromCurrency("INVALID");
        assertThatThrownBy(() -> fxDealValidator.validateFXDeal(deal))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void testValidateCurrency_InvalidFromAndToCurrency() {
        FxDealRequestDto deal = createValidDeal();
        deal.setFromCurrency("EUR");
        deal.setToCurrency("EUR");
        assertThatThrownBy(() -> fxDealValidator.validateFXDeal(deal))
                .hasMessage("From and To currencies should not be the same")
                .isInstanceOf(InvalidDealRequestException.class);
    }

    @Test
    void testValidatePositiveAmount_NegativeAmount() {
        FxDealRequestDto deal = createValidDeal();
        deal.setAmount(new BigDecimal("-100.0"));
        assertThatThrownBy(() -> fxDealValidator.validateFXDeal(deal))
                .hasMessage("Deal amount must be greater than zero.")
                .isInstanceOf(InvalidDealRequestException.class);
    }

    @Test
    void testValidatePositiveAmount_ZeroAmount() {
        FxDealRequestDto deal = createValidDeal();
        deal.setAmount(BigDecimal.ZERO);
        assertThatThrownBy(() -> fxDealValidator.validateFXDeal(deal))
                .hasMessage("Deal amount must be greater than zero.")
                .isInstanceOf(InvalidDealRequestException.class);
    }

    private FxDealRequestDto createValidDeal() {
        FxDealRequestDto deal = new FxDealRequestDto();
        deal.setUniqueId("DEAL123");
        deal.setFromCurrency("USD");
        deal.setToCurrency("EUR");
        deal.setAmount(new BigDecimal("100.0"));
        return deal;
    }
}
