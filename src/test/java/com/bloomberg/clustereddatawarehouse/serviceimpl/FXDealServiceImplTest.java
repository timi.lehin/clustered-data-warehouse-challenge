package com.bloomberg.clustereddatawarehouse.serviceimpl;

import com.bloomberg.clustereddatawarehouse.dtos.FxDealRequestDto;
import com.bloomberg.clustereddatawarehouse.exceptions.DealNotFoundException;
import com.bloomberg.clustereddatawarehouse.exceptions.DuplicateFXDealException;
import com.bloomberg.clustereddatawarehouse.exceptions.InvalidDealRequestException;
import com.bloomberg.clustereddatawarehouse.models.FxDeal;
import com.bloomberg.clustereddatawarehouse.repositories.FxDealRepository;
import com.bloomberg.clustereddatawarehouse.services.serviceimplementation.FxDealServiceImplementation;
import com.bloomberg.clustereddatawarehouse.utils.FxDealValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class FXDealServiceImplTest {

    @InjectMocks
    private FxDealServiceImplementation fxDealService;

    @Mock
    private FxDealRepository fxDealRepository;

    @Mock
    private FxDealValidator fxDealValidator;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
     void testSaveFXDeal_ValidDeal() throws InvalidDealRequestException {
        FxDealRequestDto validDeal = createValidDealDto();
        Currency fromCurrency = Currency.getInstance(validDeal.getFromCurrency());
        Currency toCurrency = Currency.getInstance(validDeal.getToCurrency());

        when(fxDealRepository.findByUniqueId(validDeal.getUniqueId())).thenReturn(Optional.empty());
        doNothing().when(fxDealValidator).validateFXDeal(validDeal);
        when(fxDealRepository.save(any(FxDeal.class))).thenReturn(createValidFXDeal(fromCurrency, toCurrency));

        FxDealRequestDto savedDeal = fxDealService.saveFXDeal(validDeal);

        assertThat(savedDeal).isNotNull();
        assertThat(savedDeal.getUniqueId()).isEqualTo(validDeal.getUniqueId());
    }

    @Test
     void testSaveFXDeal_DuplicateDeal() {
        FxDealRequestDto duplicateDeal = createValidDealDto();
        Currency fromCurrency = Currency.getInstance(duplicateDeal.getFromCurrency());
        Currency toCurrency = Currency.getInstance(duplicateDeal.getToCurrency());

        when(fxDealRepository.findByUniqueId(duplicateDeal.getUniqueId())).thenReturn(Optional.of(createValidFXDeal(fromCurrency, toCurrency)));
        doNothing().when(fxDealValidator).validateFXDeal(duplicateDeal);

        assertThatThrownBy(() -> fxDealService.saveFXDeal(duplicateDeal))
                .hasMessage("FX Deal already exists")
                .isInstanceOf(DuplicateFXDealException.class);
    }

    @Test
     void testSaveFXDeal_InvalidDeal() {
        FxDealRequestDto invalidDeal = createInvalidDealDto();

        doThrow(InvalidDealRequestException.class).when(fxDealValidator).validateFXDeal(invalidDeal);

        assertThatThrownBy(() -> fxDealService.saveFXDeal(invalidDeal))
                .isInstanceOf(InvalidDealRequestException.class);
    }

    @Test
     void testFetchFXDeal_ExistingDeal() {
        String dealId = "DEAL123";
        Currency fromCurrency = Currency.getInstance("USD");
        Currency toCurrency = Currency.getInstance("EUR");
        when(fxDealRepository.findByUniqueId(dealId)).thenReturn(Optional.of(createValidFXDeal(fromCurrency, toCurrency)));
        FxDealRequestDto fetchedDeal = fxDealService.fetchFXDeal(dealId);

        assertThat(fetchedDeal).isNotNull();
        assertThat(fetchedDeal.getUniqueId()).isEqualTo(dealId);
    }

    @Test
     void testFetchFXDeal_NonExistingDeal() {
        String dealId = "NOT_EXIST";
        when(fxDealRepository.findByUniqueId(dealId)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> fxDealService.fetchFXDeal(dealId))
                .hasMessage("FX Deal with id " + dealId + " is not found")
                .isInstanceOf(DealNotFoundException.class);
    }

    private FxDealRequestDto createValidDealDto() {
        FxDealRequestDto deal = new FxDealRequestDto();
        deal.setUniqueId("DEAL123");
        deal.setFromCurrency("USD");
        deal.setToCurrency("EUR");
        deal.setAmount(new BigDecimal("100.0"));
        return deal;
    }
    @Test
    void getAllFXDeals_ReturnsListOfFxDeals() {
        // Arrange
        List<FxDeal> mockFxDeals = Arrays.asList(
                createSampleFxDeal(),
                createSampleFxDeal(),
                createSampleFxDeal()
        );

        when(fxDealRepository.findAll()).thenReturn(mockFxDeals);

        // Act
        List<FxDeal> result = fxDealService.getAllFXDeals();

        // Assert
        assertEquals(mockFxDeals, result);
        verify(fxDealRepository, times(1)).findAll();
    }

    @Test
    void getAllFXDeals_ReturnsEmptyList() {
        // Arrange
        when(fxDealRepository.findAll()).thenReturn(Collections.emptyList());

        // Act
        List<FxDeal> result = fxDealService.getAllFXDeals();

        // Assert
        assertTrue(result.isEmpty());
        verify(fxDealRepository, times(1)).findAll();
    }

    // Add more tests for other scenarios and edge cases


    private FxDealRequestDto createInvalidDealDto() {
        FxDealRequestDto deal = new FxDealRequestDto();
        deal.setUniqueId("DEAL123");
        // Missing fromCurrency and toCurrency
        return deal;
    }

    private FxDeal createValidFXDeal(Currency fromCurrency, Currency toCurrency) {
        return FxDeal.builder()
                .uniqueId("DEAL123")
                .amount(new BigDecimal("100.0"))
                .fromCurrency(fromCurrency)
                .toCurrency(toCurrency)
                .build();
    }

    public static FxDeal createSampleFxDeal() {
        FxDeal fxDeal = new FxDeal();
        fxDeal.setId(1L);  // Assuming an ID of 1 for simplicity
        fxDeal.setUniqueId("ABC123");
        fxDeal.setAmount(new BigDecimal("1000.00"));
        fxDeal.setFromCurrency(Currency.getInstance("USD"));
        fxDeal.setToCurrency(Currency.getInstance("EUR"));
        fxDeal.setTimestamp(LocalDateTime.now());
        return fxDeal;
    }
}
