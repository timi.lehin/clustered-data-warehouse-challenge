package com.bloomberg.clustereddatawarehouse.controllers;

import com.bloomberg.clustereddatawarehouse.dtos.FxDealRequestDto;
import com.bloomberg.clustereddatawarehouse.models.FxDeal;
import com.bloomberg.clustereddatawarehouse.services.FxDealService;
import com.bloomberg.clustereddatawarehouse.utils.ApiResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Currency;
import java.util.List;

import static com.bloomberg.clustereddatawarehouse.constants.ExceptionMessages.RETRIEVED_ALL_FX_DEALS;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FxDealControllerTest {

    private FxDealService fxDealService;
    private FxDealController controller;

    @BeforeEach
    void setUp() {
        fxDealService = mock(FxDealService.class);
        controller = new FxDealController(fxDealService);
    }

    @Test
    void saveFXDeal_ValidInput_ReturnsCreatedStatus() {
        // Arrange
        FxDealRequestDto validFxDealRequestDto = createValidFxDealRequestDto();
        when(fxDealService.saveFXDeal(validFxDealRequestDto)).thenReturn(validFxDealRequestDto);

        ResponseEntity<ApiResponse> response = controller.saveFXDeal(validFxDealRequestDto);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.CREATED, response.getBody().getStatus());
        assertEquals(validFxDealRequestDto, response.getBody().getData());
    }

    @Test
    void getFXDeal_ValidDealId_ReturnsOkStatus() {
        String validDealId = "validDealId";
        FxDealRequestDto validFxDealRequestDto = createValidFxDealRequestDto();
        when(fxDealService.fetchFXDeal(validDealId)).thenReturn(validFxDealRequestDto);

        ResponseEntity<ApiResponse> response = controller.getFXDeal(validDealId);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getBody().getStatus());
        assertEquals(validFxDealRequestDto, response.getBody().getData());
    }

    @Test
    void getAllFXDeals_ReturnsOkStatus() {
        List<FxDeal> mockFxDeals = Arrays.asList(
                createValidFxDeal(),
                createValidFxDeal(),
                createValidFxDeal()
        );

        when(fxDealService.getAllFXDeals()).thenReturn(mockFxDeals);

        // Act
        ResponseEntity<ApiResponse> response = controller.getAllFXDeals();

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getBody().getStatus());
        assertEquals(RETRIEVED_ALL_FX_DEALS, response.getBody().getMessage());
        assertEquals(mockFxDeals, response.getBody().getData());
    }

    @Test
    void getAllFXDeals_EmptyList_ReturnsOkStatus() {
        // Arrange
        when(fxDealService.getAllFXDeals()).thenReturn(Collections.emptyList());

        // Act
        ResponseEntity<ApiResponse> response = controller.getAllFXDeals();

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getBody().getStatus());
        assertEquals(RETRIEVED_ALL_FX_DEALS, response.getBody().getMessage());
//        assertTrue(response.getBody().getData().isEmpty());
    }


    private FxDealRequestDto createInvalidFxDealRequestDto() {
        FxDealRequestDto deal = new FxDealRequestDto();
        deal.setUniqueId("DEAL123");
        deal.setFromCurrency("USD");
        deal.setToCurrency("EUR");
        deal.setAmount(new BigDecimal("-100.0"));
        return deal;
    }


    private FxDealRequestDto createValidFxDealRequestDto() {
        FxDealRequestDto deal = new FxDealRequestDto();
        deal.setUniqueId("DEAL123");
        deal.setFromCurrency("USD");
        deal.setToCurrency("EUR");
        deal.setAmount(new BigDecimal("100.0"));
        return deal;
    }

    private FxDeal createValidFxDeal() {
        FxDeal deal = new FxDeal();
        deal.setUniqueId("DEAL123");
        deal.setFromCurrency(Currency.getInstance("USD"));
        deal.setToCurrency(Currency.getInstance("EUR"));
        deal.setAmount(new BigDecimal("100.0"));
        return deal;
    }
}
