package com.bloomberg.clustereddatawarehouse.constants;

public class ExceptionMessages {
    public static final String FX_DEAL_SAVED_SUCCESSFULLY = "FX Deal saved successfully with uniqueId: {}";
    public static final String SAVING_FX_DEAL_PAYLOAD = "Saving FX Deal. Payload: {}";
    public static final String DUPLICATE_FX_DEAL = "FX Deal already exists";
    public static final String FX_DEAL_NOT_FOUND = "FX Deal with id {} not found";
    public static final String FX_DEAL_SUCCESSFUL_MESSAGE = "FXDeal Dto created";
    public static final String CONVERTING_FX_DEALS = "Converting FX Deal to its DTO";
    public static final String INSIDE_FX_DEAL_LOG = "Inside fetchFXDeal method for dealId: {}";
    public static final String RETRIEVING_ALL_FX_DEALS = "Retrieving all FxDeals";

    public static final String RETRIEVED_ALL_FX_DEALS = "Retrieved all Fx Deals";

}
