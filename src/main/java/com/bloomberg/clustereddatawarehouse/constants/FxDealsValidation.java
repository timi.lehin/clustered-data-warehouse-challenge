package com.bloomberg.clustereddatawarehouse.constants;


public class FxDealsValidation {

    // Log messages constants
    public static final String LOG_VALIDATING_FX_DEAL = "Validating FX Deal: {}";
    public static final String LOG_FROM_TO_CURRENCY_SAME = "From and To currencies should not be the same";
    public static final String LOG_UNIQUE_ID_NULL_EMPTY = "FX Deal unique ID cannot be null or empty";
    public static final String LOG_REQUEST_NULL = "FX Deal request should not be null";
    public static final String LOG_INVALID_CURRENCY_ISO = "Invalid Currency ISO {}";
    public static final String LOG_INVALID_AMOUNT = "Deal amount must be greater than zero.";

}
