package com.bloomberg.clustereddatawarehouse.constants;
public final class ControllerLogMessages {

    public static final String LOG_SAVED_FX_DEAL = "FX Deal saved successfully";
    public static final String LOG_RETRIEVED_FX_DEAL = "FX Deal retrieved successfully";
    public static final String LOG_RETRIEVED_FX_DEAL_WITH_ID = "FX Deal retrieved successfully. UniqueId: {}";
    public static final String LOG_RETRIEVING_FX_DEAL = "Fetching FX Deal for dealId: {}";

    public static final String LOG_SAVING_FX_DEAL = "Saving FX Deal. Payload: {}";
    public static final String LOG_FX_DEAL_SAVED_SUCCESSFULLY = "FX Deal saved successfully with uniqueId: {}";

}
