package com.bloomberg.clustereddatawarehouse.controllers;

import com.bloomberg.clustereddatawarehouse.dtos.FxDealRequestDto;
import com.bloomberg.clustereddatawarehouse.models.FxDeal;
import com.bloomberg.clustereddatawarehouse.services.FxDealService;
import com.bloomberg.clustereddatawarehouse.utils.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.bloomberg.clustereddatawarehouse.constants.ControllerLogMessages.*;
import static com.bloomberg.clustereddatawarehouse.constants.ExceptionMessages.*;

@RestController
@RequestMapping("api/v1/fx-deals")
@RequiredArgsConstructor
@Slf4j
public class FxDealController {

    private final FxDealService fxDealService;

    @PostMapping("/save")
    public ResponseEntity<ApiResponse> saveFXDeal(@RequestBody FxDealRequestDto fxDealRequestDto) {
        log.info(LOG_SAVING_FX_DEAL, fxDealRequestDto);
        FxDealRequestDto dealResponseDto = fxDealService.saveFXDeal(fxDealRequestDto);
        log.info(LOG_FX_DEAL_SAVED_SUCCESSFULLY, dealResponseDto.getUniqueId());
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(ApiResponse.builder().data(dealResponseDto)
                        .status(HttpStatus.CREATED)
                        .message(LOG_SAVED_FX_DEAL)
                        .build());
    }


    @GetMapping("/{dealId}")
    public ResponseEntity<ApiResponse> getFXDeal(@PathVariable String dealId) {
        log.info(LOG_RETRIEVING_FX_DEAL, dealId);
        FxDealRequestDto deal = fxDealService.fetchFXDeal(dealId);
        log.info(LOG_RETRIEVED_FX_DEAL_WITH_ID, deal.getUniqueId());

        ApiResponse response = ApiResponse.builder()
                .data(deal)
                .status(HttpStatus.OK)  // Set the HTTP status in the ApiResponse
                .message(LOG_RETRIEVED_FX_DEAL)
                .build();

        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<ApiResponse> getAllFXDeals() {
        log.info(RETRIEVING_ALL_FX_DEALS);
        List<FxDeal> allDeals = fxDealService.getAllFXDeals();

        ApiResponse response = ApiResponse.builder()
                .data(allDeals)
                .status(HttpStatus.OK)
                .message(RETRIEVED_ALL_FX_DEALS)
                .build();
        log.info(RETRIEVED_ALL_FX_DEALS);
        return ResponseEntity.ok(response);
    }

}


