package com.bloomberg.clustereddatawarehouse.utils;

import com.bloomberg.clustereddatawarehouse.dtos.FxDealRequestDto;
import com.bloomberg.clustereddatawarehouse.exceptions.InvalidDealRequestException;
import com.google.common.base.Strings;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Currency;
import java.util.Objects;

import static com.bloomberg.clustereddatawarehouse.constants.FxDealsValidation.*;

@Component
@Slf4j
public class FxDealValidator {


    public void validateFXDeal(FxDealRequestDto fxDeal) {
        log.info(LOG_VALIDATING_FX_DEAL, fxDeal);
        validateNotNull(fxDeal);
        validateUniqueId(fxDeal.getUniqueId());
        validateCurrency(fxDeal.getFromCurrency());
        validateCurrency(fxDeal.getToCurrency());
        validateFromAndToCurrency(fxDeal.getFromCurrency(), fxDeal.getToCurrency());
        validatePositiveAmount(fxDeal.getAmount());
    }

    private void validatePositiveAmount(BigDecimal amount) {
        if (amount == null || amount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new InvalidDealRequestException(LOG_INVALID_AMOUNT);
        }
    }
    private void validateFromAndToCurrency(String fromCurrencyISO, String toCurrencyISO) {
        if (fromCurrencyISO.equals(toCurrencyISO)) {
            throw new InvalidDealRequestException(LOG_FROM_TO_CURRENCY_SAME);
        }
    }

    private void validateUniqueId(String dealUniqueId) {
        if (Strings.isNullOrEmpty(dealUniqueId)) {
            throw new InvalidDealRequestException(LOG_UNIQUE_ID_NULL_EMPTY);
        }
    }

    private void validateNotNull(Object value) {
        if (Objects.isNull(value)) {
            throw new InvalidDealRequestException(LOG_REQUEST_NULL);
        }
    }

    private void validateCurrency(String currency) {
        if (Strings.isNullOrEmpty(currency) || isValidCurrencyCode(currency)) {
            throw new InvalidDealRequestException(MessageFormat.format(LOG_INVALID_CURRENCY_ISO, currency));
        }
    }

    private boolean isValidCurrencyCode(String currencyISO) {
        return Currency.getAvailableCurrencies().stream()
                .noneMatch(v -> v.getCurrencyCode().equals(currencyISO));
    }
}

