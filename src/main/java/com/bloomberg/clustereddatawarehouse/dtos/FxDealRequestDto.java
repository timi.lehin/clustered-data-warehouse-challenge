package com.bloomberg.clustereddatawarehouse.dtos;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class FxDealRequestDto {
    private String uniqueId;
    private String fromCurrency;
    private String toCurrency;
    private LocalDateTime timestamp;
    private BigDecimal amount;
}
