package com.bloomberg.clustereddatawarehouse.services;

import com.bloomberg.clustereddatawarehouse.dtos.FxDealRequestDto;
import com.bloomberg.clustereddatawarehouse.exceptions.InvalidDealRequestException;
import com.bloomberg.clustereddatawarehouse.models.FxDeal;

import java.util.List;

public interface FxDealService {

    FxDealRequestDto saveFXDeal(FxDealRequestDto dealRequestDto) throws InvalidDealRequestException;

    FxDealRequestDto fetchFXDeal(String dealId);

    List<FxDeal> getAllFXDeals();
}