package com.bloomberg.clustereddatawarehouse.services.serviceimplementation;

import com.bloomberg.clustereddatawarehouse.dtos.FxDealRequestDto;
import com.bloomberg.clustereddatawarehouse.exceptions.DealNotFoundException;
import com.bloomberg.clustereddatawarehouse.exceptions.DuplicateFXDealException;
import com.bloomberg.clustereddatawarehouse.exceptions.InvalidDealRequestException;
import com.bloomberg.clustereddatawarehouse.models.FxDeal;
import com.bloomberg.clustereddatawarehouse.repositories.FxDealRepository;
import com.bloomberg.clustereddatawarehouse.services.FxDealService;
import com.bloomberg.clustereddatawarehouse.utils.FxDealValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Currency;
import java.util.List;
import java.util.Optional;

import static com.bloomberg.clustereddatawarehouse.constants.ExceptionMessages.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class FxDealServiceImplementation implements FxDealService {

    private final FxDealRepository fxDealRepository;
    private final FxDealValidator fxDealValidator;
    @Override
    public FxDealRequestDto saveFXDeal(FxDealRequestDto dealRequestDto) throws InvalidDealRequestException {
        log.info(SAVING_FX_DEAL_PAYLOAD, dealRequestDto);

        fxDealValidator.validateFXDeal(dealRequestDto);
        Optional<FxDeal> optionalFXDeal = fxDealRepository.findByUniqueId(dealRequestDto.getUniqueId());

        if(optionalFXDeal.isPresent()) throw new DuplicateFXDealException(DUPLICATE_FX_DEAL);

        Currency fromCurrency = Currency.getInstance(dealRequestDto.getFromCurrency());
        Currency toCurrency = Currency.getInstance(dealRequestDto.getToCurrency());

        FxDeal deal = FxDeal.builder()
                .uniqueId(dealRequestDto.getUniqueId())
                .amount(dealRequestDto.getAmount())
                .fromCurrency(fromCurrency)
                .toCurrency(toCurrency)
                .timestamp(dealRequestDto.getTimestamp())
                .build();


        FxDeal savedDeal = fxDealRepository.save(deal);
        log.info(FX_DEAL_SAVED_SUCCESSFULLY, savedDeal.getUniqueId());
        return fxDealToDto(savedDeal);
    }

    @Override
    public FxDealRequestDto fetchFXDeal(String dealId) {
        log.info(INSIDE_FX_DEAL_LOG, dealId);
        FxDeal fxDeal = fxDealRepository.findByUniqueId(dealId)
                .orElseThrow(() -> {
                    log.error(FX_DEAL_NOT_FOUND, dealId);
                    return new DealNotFoundException("FX Deal with id " + dealId + " is not found");
                });

        log.info(FX_DEAL_NOT_FOUND, dealId);
        return fxDealToDto(fxDeal);
    }

    @Override
    public List<FxDeal> getAllFXDeals() {
        return fxDealRepository.findAll();
    }

    private FxDealRequestDto fxDealToDto (FxDeal fxDeal){
        log.info(CONVERTING_FX_DEALS);
        FxDealRequestDto fxDealDto = new FxDealRequestDto();
        fxDealDto.setFromCurrency(fxDeal.getFromCurrency().getCurrencyCode());
        fxDealDto.setToCurrency(fxDeal.getToCurrency().getCurrencyCode());
        BeanUtils.copyProperties(fxDeal, fxDealDto);
        log.info(FX_DEAL_SUCCESSFUL_MESSAGE);
        return fxDealDto;
    }
}