package com.bloomberg.clustereddatawarehouse.repositories;

import com.bloomberg.clustereddatawarehouse.models.FxDeal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FxDealRepository extends JpaRepository<FxDeal, Long> {
    Optional<FxDeal> findByUniqueId(String dealId);
}
