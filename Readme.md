# ClusteredData Warehouse

This report outlines the successful fulfillment of the requirements for the ClusteredData Warehouse project. The project, part of a Scrum team initiative for Bloomberg, focuses on developing a data warehouse to analyze FX deals. The primary objective is to accept deal details, validate them, and persist them into a relational database.

## Requirements

### 1. Request Logic

#### Fields
- Deal Unique Id
- From Currency ISO Code
- To Currency ISO Code
- Deal Timestamp
- Deal Amount in ordering currency

#### Validation
Implemented logic for handling missing fields, type formats, and other essential validations. A comprehensive validation approach has been integrated for robust data processing.

#### Duplicate Check
The system successfully rejects the import of the same request twice, ensuring data integrity and preventing duplicates.

#### No Rollback
Imported rows are securely saved in the database without the possibility of rollback, meeting the specified project requirement.

### 2. Delivered

#### Relational Database (Postgres)
The project utilizes a Postgres relational database to store FX deal details securely.

#### Workable Deployment with Docker Compose
A Docker Compose configuration has been provided, enabling a seamless deployment of the application along with a sample file.

#### Maven Project
The complete source code is organized as a Maven project, ensuring easy dependency management and project build.

#### Proper Error/Exception Handling
The application is designed with robust error and exception handling mechanisms, providing clear and informative messages for efficient troubleshooting.

#### Proper Logging
Detailed logs have been implemented, offering insights into the application's behavior for debugging and monitoring purposes.

#### Comprehensive Unit Testing
A comprehensive suite of unit tests has been developed, ensuring the reliability and correctness of the application. Test coverage is respectable, covering critical aspects of the codebase.

#### Documentation Using Markdown (md)
This README file serves as comprehensive documentation, providing users with clear instructions for setting up, deploying, and understanding the project.

#### Git Repository
The source code is hosted and versioned via Git, offering version control and collaborative development capabilities.

#### Makefile 
For added convenience, a Makefile has been provided, streamlining essential tasks such as building, running, and testing the application.



## Run Locally via Make
To run this application, you will need to clone the repository and aso have Docker installed.


Clone the project

```bash
git clone git@gitlab.com:timi.lehin/clustered-data-warehouse-challenge.git
git clone https://gitlab.com/timi.lehin/clustered-data-warehouse-challenge.git
```

Enter into the project directory by running:

```bash
cd clustereddatawarehouse
```

Build the project

```bash
make build
```

Run the project

```bash
make run
```

Stop the project

```bash
make stop
```

Test the project

```bash
make test
```

Swagger URL for API documentation when the server is running:

```bash
http://localhost:8081/swagger-ui/index.html
```


## API Reference
All URLs are relative to:
- for `docker` http://localhost:8081
- for running it via `terminal` http://localhost:8080

### Save FX Deal

```http
POST /api/v1/fx-deals
```

| Method | HTTP Request         | Description                  |
|--------|----------------------|------------------------------|
| `POST` | `/api/v1/fx-deals`   | Endpoint for saving FX deals |

#### Sample Request
```json
{
  "uniqueId": "abc123",
  "fromCurrency": "USD",
  "toCurrency": "EUR",
  "timestamp": "2024-01-09T12:30:45",
  "amount": 1000.50
}

```

#### Sample Response
```json
{
  "message": "FX Deal retrieved successfully",
  "data": {
    "uniqueId": "abc123",
    "fromCurrency": "USD",
    "toCurrency": "EUR",
    "timestamp": "2024-01-09T12:30:45",
    "amount": 1000.5
  }
}
```

### Get FX Deal by ID

```http
GET /api/v1/fx-deals/{dealId}
```

| Method | HTTP Request                   | Description          |
|--------|--------------------------------|----------------------|
| `GET`  | `/api/v1/fx-deals/{dealId}`    | To get a single Deal |

#### Sample Request
``` 
localhost:8081/api/v1/fx-deals/abc123
```

#### Sample Response
```json
{
  "message": "FX Deal retrieved successfully",
  "data": {
    "uniqueId": "abc123",
    "fromCurrency": "USD",
    "toCurrency": "EUR",
    "timestamp": "2024-01-09T12:30:45",
    "amount": 1000.5
  }
}
```

### Get All FX Deals

```http
GET /api/v1/fx-deals
```

| Method | HTTP Request          | Description                           |
|--------|-----------------------|---------------------------------------|
| `GET`  | `/api/v1/fx-deals`    | To get all FxDeals present in the DB  |

#### Sample Request
``` 
localhost:8081/api/v1/fx-deals
```

#### Sample Response
```json
{
  "message": "Retrieved all Fx Deals",
  "status": "OK",
  "data": [
    {
      "id": 1,
      "uniqueId": "PQ-ytd99",
      "amount": 10000,
      "fromCurrency": "GBP",
      "toCurrency": "NGN",
      "timestamp": "2023-09-16T12:02:24.680682"
    },
    {
      "id": 2,
      "uniqueId": "1234",
      "amount": 10000,
      "fromCurrency": "GBP",
      "toCurrency": "NGN",
      "timestamp": "2023-09-16T12:02:24.680682"
    },
    {
      "id": 3,
      "uniqueId": "abc",
      "amount": 10000,
      "fromCurrency": "GBP",
      "toCurrency": "NGN",
      "timestamp": "2023-09-16T12:02:24.680682"
    },
    {
      "id": 4,
      "uniqueId": "abc123",
      "amount": 1000.5,
      "fromCurrency": "USD",
      "toCurrency": "EUR",
      "timestamp": "2024-01-09T12:30:45"
    },
    {
      "id": 5,
      "uniqueId": "abc1234",
      "amount": 1000.5,
      "fromCurrency": "USD",
      "toCurrency": "EUR",
      "timestamp": "2024-01-09T12:30:45"
    }
  ]
}
```