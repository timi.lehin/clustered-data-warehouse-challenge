build:
	chmod +x ./mvnw
	./mvnw clean install -DskipTests

run: build
	docker-compose up

stop:
	docker-compose down

test: build
	./mvnw test